########################################################################################
#          EXAMPLE FILE for the VIMMP ONTOLOGY OF VARIABLES - VOV                      #
#                                                                                      #
# This file contains examples of statements that can be made with "vov.ttl"            #
#                                                                                      #
# Authors of the example                                                               #
# Silvia Chiacchiera, Martin T. Horsch (STFC/UKRI, Daresbury Laboratory, UK)           #
#                                                                                      #
# Contributors of the example:                                                         #
# Gianluca Boccardo (Politecnico di Torino, Italy)                                     #
#                                                                                      #
# last modified: 6 November 2020                                                       #
#                                                                                      #
########################################################################################

@prefix ex: <https://purl.vimmp.eu/semantics/viso/example_vov.ttl#>.

@prefix vov: <https://purl.vimmp.eu/semantics/vov/vov.ttl#>.
@prefix viso: <https://purl.vimmp.eu/semantics/viso/viso-general.ttl#>.
@prefix viso-am: <https://purl.vimmp.eu/semantics/viso/viso-atomistic-mesoscopic.ttl#>.
@prefix viso-el: <https://purl.vimmp.eu/semantics/viso/viso-electronic.ttl#>.
@prefix viso-co: <https://purl.vimmp.eu/semantics/viso/viso-continuum.ttl#>.
@prefix owl: <http://www.w3.org/2002/07/owl#>.
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xs: <http://www.w3.org/2001/XMLSchema#>.
@prefix osmo: <https://purl.vimmp.eu/semantics/osmo/osmo.ttl#>.
@prefix evmpo: <https://emmc.eu/semantics/evmpo/evmpo.ttl#>.
@prefix qudt: <http://qudt.org/schema/qudt/>. 
@prefix qudt-qk: <http://qudt.org/vocab/quantitykind/>. 
@prefix viprs: <https://purl.vimmp.eu/semantics/alignment/viprs.ttl#>.
@prefix molmod: <https://purl.vimmp.eu/semantics/scenario/molecular-models/molecular-models.ttl#>.

<https://purl.vimmp.eu/semantics/vov/example_vov.ttl#> a owl:Ontology.

ex:SITE_A vov:has_mass ex:MASS_A;
  vov:has_electric_charge ex:CHARGE_A;
  vov:has_electric_dipole_moment ex:DPL_A;
  vov:has_electric_quadrupole_moment ex:LQPL_A;
  vov:has_position ex:R_A.

ex:LQPL_A a vov:electric_linear_quadrupole_moment;
  vov:has_modulus ex:LQPL_A_magnitude;
  vov:has_direction ex:LQPL_A_orientation.

ex:R_AO a vov:relative_position;
  vov:has_initial_point ex:site_O;
  vov:has_final_point ex:site_A;
  rdfs:comment "The position of SITE_A with respect to site_O"^^xs:string.

ex:V_AO a vov:relative_velocity;
  vov:has_initial_point ex:site_O;
  vov:has_final_point ex:site_A;
  rdfs:comment "The velocity of SITE_A with respect to site_O"^^xs:string.

ex:A_I a viso-am:mass_site;
  vov:has_species "A"^^xs:string.

ex:A_J a viso-am:mass_site;
  vov:has_species "A"^^xs:string.

ex:R_IJ a vov:relative_position;
  vov:has_initial_point ex:A_I;
  vov:has_final_point ex:A_J.

ex:HARMONIC_BONDING_POTENTIAL_AA a viso-am:stretching_potential;
  vov:involves_variable ex:K_AA, ex:R0_AA, ex:R_IJ.
		  
viso-co:NON_NEWTONIAN_POWER_LAW_STRESS_TENSOR viso:involves vov:CONSISTENCY_INDEX,
  vov:POWER_LAW_INDEX, vov:MIN_VISCOSITY, vov:MAX_VISCOSITY.

vov:POWER_LAW_INDEX a vov:variable, vov:R_IS_DIMENSIONLESS.
vov:CONSISTENCY_INDEX a vov:variable.
vov:MIN_VISCOSITY a vov:variable, vov:dynamic_viscosity.
vov:MAX_VISCOSITY a vov:variable, vov:dynamic_viscosity.

# We define some named individuals to visualize more variables via the DL_QUERY 

ex:INTEGRATOR_123 a viso-am:integrator.

ex:POTENTIAL_123 a viso-am:mie_potential.

ex:MESH_123 a viso-co:continuum_mesh.

ex:BASIS_SET_123 a viso-el:basis_set.

#################################################################################
#          Example of statements about the molmod scenario                      #
#################################################################################

molmod:NH3_PARAMETER_SIG vov:shares_role_with vov:MIE_SIGMA.
molmod:NH3_PARAMETER_EPS vov:shares_role_with vov:MIE_EPSILON.
molmod:NH3_PARAMETER_M vov:shares_role_with vov:MIE_REPULSIVE_EXPONENT.
molmod:NH3_PARAMETER_N vov:shares_role_with vov:MIE_ATTRACTIVE_EXPONENT.

molmod:NH3_PARAMETER_SITE_A_RAD a vov:relative_position;
  vov:has_initial_point molmod:NH3_SITE_COM;
  vov:has_final_point molmod:NH3_SITE_A.

molmod:NH3_PARAMETER_SITE_B_RAD a vov:relative_position;
  vov:has_initial_point molmod:NH3_SITE_COM;
  vov:has_final_point molmod:NH3_SITE_B.

molmod:NH3_SITE_A vov:has_mass molmod:MASS_SITE_A.
molmod:NH3_SITE_B vov:has_mass molmod:MASS_SITE_B.

molmod:MASS_SITE_A vov:shares_value_with molmod:HN3_HALF_MASS.
molmod:MASS_SITE_B vov:shares_value_with molmod:HN3_HALF_MASS.

#################################################################################
# HERE SOME STATEMENTS ARE REPEATED FOR CONVENIENCE, NOT TO LOAD VISO-AM

viso-am:mie_potential rdfs:subClassOf  viso-am:potential.
viso-am:stretching_potential rdfs:subClassOf viso-am:potential.
viso-am:electrostatic_potential rdfs:subClassOf viso-am:potential.

viso-am:potential rdfs:subClassOf viso:am_materials_relation_trait.
viso:am_materials_relation_trait rdfs:subClassOf viso:materials_relation_trait.

viso-am:integrator rdfs:subClassOf viso:am_solver_feature.
viso-am:thermostat rdfs:subClassOf viso:am_solver_feature.
viso-am:NVT a viso:am_model_feature.

viso-am:mass_site rdfs:subClassOf viso:model_object.

#################################################################################
# HERE SOME STATEMENTS ARE REPEATED FOR CONVENIENCE, NOT TO LOAD VISO-CO

viso-co:NEWTONIAN_STRESS_TENSOR a viso:co_materials_relation_trait.
viso-co:NON_NEWTONIAN_POWER_LAW_STRESS_TENSOR a viso:co_materials_relation_trait.
viso:co_materials_relation_trait rdfs:subClassOf viso:materials_relation_trait.
viso-co:continuum_mesh rdfs:subClassOf viso:co_solver_feature.

#################################################################################
# HERE SOME STATEMENTS ARE REPEATED FOR CONVENIENCE, NOT TO LOAD VISO-EL

viso-el:basis_set rdfs:subClassOf viso:el_solver_feature.

