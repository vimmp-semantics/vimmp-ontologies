## Content

Introduction and overview in the directory:

* doc/

Separate ontology files in the directories:

* alignment/
* macro/
* mmto/
* osmo/
* otras/
* vico/
* viso/
* vivo/
* vov/

Combined ontology file in the directory:

* combined/

Example scenario descriptions in the directories:

* scenario/eos-parameterization/
* scenario/hpc-job/
* scenario/molecular-models/
* scenario/self-descriptions/
* scenario/software/
* scenario/summer-school/
* scenario/training/

**Note on versioning**: the material in these folders corresponds to the *development version*,
and also contains additional auxiliary files used in the development process.
The VIMMP ontology releases are summarized in the [releases page](./releases.md) and the corresponing
cleaned-up material is in the folder:

* releases/

