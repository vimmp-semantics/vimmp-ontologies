## How to contribute

### Make suggestions, requests and contribute via GitLab

The *Issue* and *Merge Request* (MR) features of GitLab will be used
to suggest/request changes in the VIMMP Ontologies and to implement them.

The users can decide, based on their experience and preferences, how deep to go into
the technical aspects: suggestions can simply be provided as text, or creating a dedicated
branch in the repository and opening a MR.  

In any case, to organize and track the changes, GitLab Issues and MRs need be tagged using the provided
[labels](https://gitlab.com/vimmp-semantics/vimmp-ontologies/-/labels).

#### An example: suggest adding an item into a taxonomy

As an example, imagine an *alien user* would like to request the addition of a topic to the OTRAS list of materials
and modelling topics. 

* **Light approach, using GitLab Issues**
  * Step 1: Register on GitLab
  * Step 2: Open an Issue about your suggestion/request and tag it
      * Step 2a: Open an Issue, by clicking on "New issue" button on the [issues page](https://gitlab.com/vimmp-semantics/vimmp-ontologies/-/issues)
      * Step 2b: Beside a Title, provide a brief text in the Description section. For example:

         > "I think a topic is missing from the MM topic list in OTRAS, and it should be added, namely: *Extra-terrestrial materials*.   
         > Motivation: This is needed to describe our products in the recently created *ET-Platform*. Thanks!"
      * Step 2c: Tag the Issue with appropriate labels. In this case: suggestion, taxonomy, OTRAS
      * Step 2d: Assign the issue to a member of the VIMMP team

* **Advanced approach, using GitLab MR**
  * Step 1: Register on GitLab and add your SSH keys there
  * Step 2: Clone the repository
  
      ```sh
      git clone git@gitlab.com:vimmp-semantics/vimmp-ontologies.git
      ```    
  * Step 3: Create a branch
  
      ```sh
      git branch suggestion_for_OTRAS
      ```
  * Step 4: Implement your changes within the branch
       * Step 4a: Go to the newly created branch

          ```sh
          git checkout suggestion_for_OTRAS
          ```
       * Step 4b: Implement your changes, which will include .TTL statements such as:	  

          ```
          otras:mm_topic_extra_terrestrial_materials a owl:Class; 
            rdfs:comment "a topic related to extraterrestrial materials and their properties"^^xs:string; 
            rdfs:subClassOf otras:mm_topic_materials.
          ```
       * Step 4c: When appropriate, commit your changes

          ```sh
          git add <FILE_NAME>
          git commit -m <some comment>
          ```
  * Step 5:  Once you are done with your changes, push the branch back, after making sure you are are up to date with the repository.

     ```sh
     git checkout master
     git pull origin
     git push origin suggestion_for_OTRAS
     ```
  * Step 6: Open a Merge Request about your changes and tag it 
      * Step 6a: Open a MR, by clicking on "New merge request" button on the [MRs page](https://gitlab.com/vimmp-semantics/vimmp-ontologies/-/merge_requests)
      * Step 6b: Tag the MR with appropriate labels. In this case: suggestion, taxonomy, OTRAS
      * Step 6c: Assign the MR to a member of the VIMMP team

Of course, a spectrum of intermediate options is possible too, such as providing a few appropriate .TTL lines as as part of an Issue.

### Coding style

Here we just give some general indications. For more details, especially on the ontologies' structure and alignment, please see the documentation
and the ontology files.

* The VIMMP Ontologies are written in Turtle (.TTL)
* For VIMMP-defined names (both classes and individuals) the *under\_score* convention is used,
where words are separated by the symbol "\_". 
  * For classes, all letters are lower case (E.g.: *example\_class*)
  * For individuals, all letters are upper case (E.g.: *EXAMPLE\_INDIVIDUAL*) 
* All classes are sub-sumed under EVMPO categories
* All properties are sub-sumed under VIPRS properties

### Governance workflow

This Gitlab instance will serve as the primary editor (in the broad sense, as the collaborative environment where discussions and changes happen and are tracked)
for the ontologies. However requests can also be gathered from other channels (e.g., [MatPortal](https://matportal.org/), where the main releases
of VIMMP ontologies will appear too). The main activities and actors in the governance workflow are:

* Request/suggest changes in the VIMMP ontologies (Who: any interested party)
* Review and accept changes in the VIMMP ontologies (Who: VIMMP ontologies main authors)
* Assess impact of the ontology changes on the VIMMP marketplace platform. Implement platform updates if needed
(Who: VIMMP ontologies main authors and VIMMP marketplace platform operator, in communication with third parties)

Note: All requests and suggestions will be evaluated on a case-by-case basis, with no fixed frequency for ontologies and platform updates.

