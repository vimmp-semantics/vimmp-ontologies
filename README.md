# Virtual Materials Marketplace (VIMMP) Ontologies

The [Virtual Materials Marketplace (VIMMP) project](https://www.vimmp.eu/)
is developing a digital marketplace, i.e., a platform to facilitate exchanges
between providers and users in the area of materials modelling. Typical goods will include software,
modelling data, workflows, training and translation (from an industrial problem
to a modelling solution) services.

To enable semantic interoperability with external services and platforms, the VIMMP
project has developed *a system of marketplace-level domain ontologies* (see figure below),
supporting the ingest and retrieval of data and metadata at the VIMMP marketplace
frontend; these ontologies are expressed in OWL2 using Turtle (TTL) notation.
Internally, VIMMP uses the marketplace-level domain ontologies as a part of its approach to data
management, underlying the interactions with users at its frontend. To coordinate these
developments with the community and the ecosystem of platforms developed from related
projects funded from the NMBP area of the Horizon 2020 research and innovation
programme, VIMMP contributes to the activities of the European Materials Modelling
Council (EMMC), particularly the EMMC focus area of digitalization, and it employs as a top-level ontology the
[Elementary Multiperspective Material Ontology (EMMO)](https://github.com/emmo-repo/EMMO), 
previously known as the "European Materials and Modelling Ontology".

<img src="../structure.png" alt="drawing" width="500"/>

For details on the repository content, main changes between releases and on how to contribute, please
see the respective pages:

* [Content](content.md)
* [Releases](releases.md)
* [How to contribute](contributing.md)

## License

The VIMMP Ontologies are released by the VIMMP project consortium under the terms and conditions of [LGPL version 3](./LICENSE.md).

## Funding

This work was funded from the European Union's Horizon 2020 research
and innovation programme under grant agreement no. 760907, Virtual
Materials Marketplace (VIMMP).

The project time frame is 2018-2021.

## Authors and contributors

For any requests related to this work, contact its main authors.

Main authors:

*  Silvia Chiacchiera,
   silvia.chiacchiera[at]stfc.ac.uk,
   ORCID 0000-0003-0422-7870;
*  Martin Thomas Horsch,
   horsch[at]inprodat.de,
   ORCID 0000-0002-9464-6739;
*  Michael A. Seaton,
   michael.seaton[at]stfc.ac.uk,
   ORCID 0000-0002-4708-573X;
*  Ilian T. Todorov,
   ilian.todorov[at]stfc.ac.uk;
   ORCID 0000-0001-7275-1784.

Other authors and contributors:

   Barbara Andreon, Youness Bami, Esteban Bayro Kaiser,
   Gianluca Boccardo, Hauke Brüning, Paola Carbone,
   Welchy Leite Cavalcanti, Mara Chiricotto, Javier Díaz Brañas,
   Joshua D. Elliott, Éric Fayolle, Andreas Fiseni, Yvan Fournier,
   Emanuele Ghedini, Gerhard Goldbeck, Adham Hashibon,
   Natalia A. Konchakova, Peter Klein, Helge Krieg, Martin Lísal,
   Vladimir Lobaskin, Jean-Pierre Minier, Philipp Neumann,
   Christoph Niethammer, Pascale Noyret, Ignacio Pagonabarraga Mora,
   Barbora Planková, Björn Schembera, Peter Schiffels,
   Georg J. Schmitz, Andrea Scotto di Minico, Karel Šindelka,
   Vincent Stobiac, Daniele Toti, Jadran Vrabec.

The role of the other authors and contributors in collaborating on
individual ontologies and examples is mentioned in the respective TTL
files and the introduction/overview PDF document.

## Recommended literature citations

1. M. T. Horsch, S. Chiacchiera, W. L. Cavalcanti, B. Schembera, Data
   Technology in Materials Modelling,  Springer, Cham,
   ISBN 978-3-03068596-6, 2021. [[link to the publication](https://doi.org/10.1007/978-3-030-68597-3)]
2. M. T. Horsch, S. Chiacchiera, M. A. Seaton, I. T. Todorov,
   K. Šindelka, M. Lísal, B. Andreon, E. Bayro Kaiser, G. Mogni,
   G. Goldbeck, R. Kunze, G. Summer, A. Fiseni, H. Brüning,
   P. Schiffels, W. L. Cavalcanti, "Ontologies for the Virtual
   Materials Marketplace," KI - Künstliche Intelligenz 34(3), 423-428,
   doi:10.1007/s13218-020-00648-9, 2020.
   [[link to the publication](https://doi.org/10.1007/s13218-020-00648-9)]

