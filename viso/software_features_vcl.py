# Script to generate the "Zontal codelist" (skos) for software tool features.
# how to run interactively: python3 -i software_features_vcl.py
# owlready2 module is needed. I am using its version 0.13 with python 3.5.

# NB: In the first part, I use Owlready2 methods, in the last one basically work with strings.
# The aim of the first part of this script is to create the lists of classes/individuals I need.
# Then, these lists (jointlist and force_field_list) are used to create the "features-vcl.ttl" file, as a text file.

from owlready2 import *

# loading
visoonto = get_ontology("viso-reasoned.owl") # uses the reasoned (superclasses, class instances, types) owl version of VISO, can be generated via Protege
visoonto.load()

el_solver_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#el_solver_feature"]
el_model_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#el_model_feature"]

am_solver_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#am_solver_feature"]
am_model_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#am_model_feature"]

co_solver_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#co_solver_feature"]
co_model_feature_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#co_model_feature"]
execution_mode_class = IRIS["https://purl.vimmp.eu/semantics/viso/viso-general.ttl#execution_mode"]

#outfile = open("outfile_features.txt","w")
vclfile = open("features-vcl.ttl","w")

header='@prefix viso: <https://purl.vimmp.eu/semantics/viso/viso-general.ttl#>.'+ "\n" + '@prefix viso-el: <https://purl.vimmp.eu/semantics/viso/viso-electronic.ttl#>.' + "\n"+ '@prefix viso-am: <https://purl.vimmp.eu/semantics/viso/viso-atomistic-mesoscopic.ttl#>.' + "\n"+'@prefix viso-co: <https://purl.vimmp.eu/semantics/viso/viso-continuum.ttl#>.' + "\n" +'@prefix xs: <http://www.w3.org/2001/XMLSchema#>.'+ "\n" +'@prefix skos: <http://www.w3.org/2004/02/skos/core#>.'+ "\n" +'@prefix vcl: <https://purl.vimmp.eu/semantics/alignment/vcl.ttl#>.'+ "\n"+ "\n"

vclfile.write(header)

# this is solution to get a codelist for all *instances/individuals* (not classes)
# TO SEE: do we want to add entries for all (possibly empty) classes?
# In that case, the descendants command can be used (in combination with other commands).

# descendants gives all (not only direct) the sub-classes of a class
# el_solver_feature_list = (el_solver_feature_class.descendants()) 
# el_model_feature_list = (el_model_feature_class.descendants())

# am_solver_feature_list = (am_solver_feature_class.descendants())
# am_model_feature_list = (am_model_feature_class.descendants())

# co_solver_feature_list = (co_solver_feature_class.descendants())
# co_model_feature_list = (co_model_feature_class.descendants())

#execution_mode_list = (execution_mode_class.descendants()) # for some reason, it causes a core crash (?)

el_solver_feature_individuals = el_solver_feature_class.instances()
el_model_feature_individuals = el_model_feature_class.instances()
am_solver_feature_individuals = am_solver_feature_class.instances()
am_model_feature_individuals = am_model_feature_class.instances()
co_solver_feature_individuals = co_solver_feature_class.instances()
#co_model_feature_individuals = co_model_feature_class.instances()   # for some reason, it causes a core crash (?)

jointlist = [*el_solver_feature_individuals, *el_model_feature_individuals, *am_solver_feature_individuals, *am_model_feature_individuals, *co_solver_feature_individuals]
#print(jointlist)
print("the number of individual features is:",len(jointlist))

for el in jointlist:
    # print(el.name)
    # print(el.is_a)
    # print(el.iri)
    siri= (el.iri).rsplit('#')[-1] # splits IRI using '#' as separator, and takes the last part of the resulting list
    styp = str(el.is_a[0])
    styp = styp.rsplit(".")[-1]    
    styp_w = styp.replace('_',' ')
    siri_w = siri.replace('_',' ')
    if (el in el_solver_feature_individuals):
        spre="EL:"
        s= "EL solver feature: " + styp + ": " + siri  # a possible preflabel choice
        sw= "EL solver feature: " + styp_w + ": " + siri_w  # same, with white spaces
        l1 = 'viso-el:'+siri+' a vcl:codelist_software_feature;'+ "\n"
    elif (el in el_model_feature_individuals):
        spre="EL:"
        s= "EL model feature: " + styp + ": " + siri  # a possible preflabel choice
        sw= "EL model feature: " + styp_w + ": " + siri_w  # same, with white spaces
        l1 = 'viso-el:'+siri+' a vcl:codelist_software_feature;'+ "\n"
    elif (el in am_solver_feature_individuals):
        spre="AM:"
        s= "AM solver feature: " + styp + ": " + siri  # a possible preflabel choice
        sw= "AM solver feature: " + styp_w + ": " + siri_w  # same, with white spaces
        l1 = 'viso-am:'+siri+' a vcl:codelist_software_feature;'+ "\n"
    elif (el in am_model_feature_individuals):
        spre="AM:"
        s= "AM model feature: " + styp + ": " + siri  # a possible preflabel choice
        sw= "AM model feature: " + styp_w + ": " + siri_w  # same, with white spaces
        l1 = 'viso-am:'+siri+' a vcl:codelist_software_feature;'+ "\n"
    elif (el in co_solver_feature_individuals):
        spre="CO:"
        s= "CO solver feature: " + styp + ": " + siri  # a possible preflabel choice
        sw= "CO solver feature: " + styp_w + ": " + siri_w  # same, with white spaces
        l1 = 'viso-co:'+siri+' a vcl:codelist_software_feature;'+ "\n"
    else:
        print("this should not happen!", el)
    # end of if        
    l2 = '  skos:inScheme vcl:CL_SCHEME_SOFTWARE_FEATURE;' + "\n"
    l3 = '  skos:notation "'+spre+siri+'"^^xs:string, "'+spre+siri+'"@en;' + "\n"
    l4 = '  skos:prefLabel "'+sw+'"^^xs:string.' + "\n" + "\n"        
    #outfile.write(sw+"\n")
    vclfile.write(l1)
    vclfile.write(l2)
    vclfile.write(l3)
    vclfile.write(l4)

# creating a "Zontal codelist" (skos) for Force fields    
force_field_class=IRIS["https://purl.vimmp.eu/semantics/viso/viso-atomistic-mesoscopic.ttl#force_field"]
force_field_list=list(force_field_class.descendants())

force_field_list.remove(force_field_class)

for el in force_field_list:
#    print(el.name)
#    print(el.iri)
    siri= (el.iri).rsplit('#')[-1] # splits IRI using '#' as separator, and takes the last part of the resulting list
    siri_up=siri.upper()
    siri_w = siri.replace('_',' ')
    styp_w="force field"
    lab=siri.split("_")[0]
    lab=lab.upper()
    spre="AM:"
    # print(lab)
    sw= "AM model feature: " + styp_w + ": " + lab  # preflabel with white spaces
    l1 = 'viso-am:INDIVIDUAL_OF_'+siri_up + ' a viso-am:force_field, vcl:codelist_software_feature, viso-am:'+siri+";"+ "\n"
    l2 = '  skos:inScheme vcl:CL_SCHEME_SOFTWARE_FEATURE;' + "\n"
    l3 = '  skos:notation "'+spre+siri+'"^^xs:string, "'+spre+siri+'"@en;' + "\n"
    l4 = '  skos:prefLabel "'+sw+'"^^xs:string.' + "\n" + "\n"
    # print(l1)
    # print(l2)
    # print(l3)
    # print(l4)
    # outfile.write(sw+"\n")
    vclfile.write(l1)
    vclfile.write(l2)
    vclfile.write(l3)
    vclfile.write(l4)
                                                
#outfile.close()
vclfile.close()
