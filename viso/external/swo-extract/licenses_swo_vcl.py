# Script to generate the codelist for licenses.

# how to run interactively: python3 -i licenses_swo_vcl.py
# owlready2 module is needed. I am using its version 0.13 with python 3.5.

# NB:  In the first part, I use Owlready2 methods, in the last one basically work with strings.
# The aim of the first part of this script is to create the list of classes I need (all_licenses_list).
# Then, this list is used to create the "vclfile.ttl" file, as a text file.

from owlready2 import *

# loading
swoonto = get_ontology("zso6c9zs.owl") # using the extracted file. Full one can be used too
swoonto.load()

license_class = IRIS["http://www.ebi.ac.uk/swo/SWO_0000002"]

# IDEA: I would like to create individuals for each license, except the vendor-specific ones

all_licenses_list=list(license_class.descendants()) # -> 111 elements
print(len(all_licenses_list))

# list of the proper subclasses of the vendor-specific license class

vendor_specific_class = IRIS["http://www.ebi.ac.uk/swo/license/SWO_1000103"]
vendor_specific_licenses_list = list(vendor_specific_class.descendants())
vendor_specific_licenses_list.remove(vendor_specific_class)

for el in vendor_specific_licenses_list:
    if el in all_licenses_list: 
        print('removing' + str(el.label))
        all_licenses_list.remove(el)

all_licenses_list.remove(license_class) #remove the class itself from the list

print(len(all_licenses_list)) # -> 67 elements :-)


# # To create individuals

# count = 0
# for el in all_licenses_list:
#     count +=1    
#     license_class('individual_of_'+el.name, namespace=swoonto, label=el.label) # creates individuals of <class>, called "individual_of_<class>"
#     print(el.label)
#     print(el.iri)

# print(count)
    
# ordering by iri

def iri_fun(x):
    return x.iri

all_licenses_list.sort(key=iri_fun)

for el in all_licenses_list:
    print(el.iri)

# ordering by label
def label_fun(x):
    return x.label

all_licenses_list.sort(key=label_fun)

for el in all_licenses_list:
    el.label.sort(key=len) # if muliple labels are present, sorts them by length
    print(el.label)        
    
# # Creating an output file with some info

# outfile = open("outfile.txt","w")

# for el in all_licenses_list:
#     #outfile.write(str(el.label)) # not formatted
#     sname= (el.iri).rsplit('/')[-1]
#     prefix = (el.iri).rsplit('/')[-2]
#     s= str(el.label[0])+',  '+  sname + ',  ' + prefix + "\n" 
#     outfile.write(s)
    
# outfile.close()

# Creating a .ttl with the individuals needed for the code-list
# They are sorted by (their shortest) label, alphabetically

vclfile = open("licenses-swo-vcl.ttl","w")

header='@prefix viso: <https://purl.vimmp.eu/semantics/viso/viso-general.ttl#>.'+ "\n"+'@prefix xs: <http://www.w3.org/2001/XMLSchema#>.'+ "\n"+'@prefix swo: <http://www.ebi.ac.uk/swo/>.'+ "\n"+'@prefix swo-license: <http://www.ebi.ac.uk/swo/license/>.'+ "\n"+'@prefix skos: <http://www.w3.org/2004/02/skos/core#>.'+ "\n"+'@prefix vcl: <https://purl.vimmp.eu/semantics/alignment/vcl.ttl#>.'+ "\n"+ "\n"

vclfile.write(header)

for el in all_licenses_list:
    sname= (el.iri).rsplit('/')[-1] # splits IRI using '/' as separator, and takes the last part of the resulting list
    lab=el.label[0] # takes the first element of the label
    lab_nw=(el.label[0]).replace(' ','_') # label with no white spaces (nw)
    l1 = 'viso:INDIVIDUAL_OF_'+sname + ' a viso:license, vcl:codelist_license, swo-license:'+sname+";"+ "\n" 
    l2 = '  skos:inScheme vcl:CL_LICENSE;' + "\n"
    l3 = '  skos:notation "'+lab_nw+'"^^xs:string, "'+lab_nw+'"@en;' + "\n" 
    l4 = '  skos:prefLabel "'+lab+'"^^xs:string.' + "\n" + "\n" 
    vclfile.write(l1)
    vclfile.write(l2)
    vclfile.write(l3)
    vclfile.write(l4)

vclfile.close()

