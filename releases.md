## Releases

Note: we indicate the releases by their date, since there is no global versioning for
the set of VIMMP ontologies, and each has its own version number.

### 2021-01-02

Revision.

- Minor fixes and updates

Main additions: 

- In VIPRS: class *viprs:conceptualization* and mereosemiotic property *viprs:is_subsign_of* (and its inverse *viprs:has_subsign*)
- In OSMO: an additional section type, the *osmo:simulation_overview*
- In OSMO, VISO, etc: various "codelists" (SKOS version of options for Zontal dropdown menus)
- Examples: HPC-job scenario and MolMod DB interoperability example

### 2020-07-06

Major release of the complete set of VIMMP ontologies and their documentation, in
correspondence with the VIMMP project deliverable D1.4.

This was the result of the work done since the start of the project on 2018; before this release
the ontologies had been already partially presented (in workshops, conferences, seminars etc) and published
at various stages of development.

The set includes eight marketplace-level ontologies

* MACRO: Marketplace-Accessible Computational Resource Ontology
* MMTO: Materials Modelling Translation Ontology
* OSMO: Ontology for Simulation, Modelling, and Optimization
* OTRAS: Ontology for TRAining Services 
* VICO: VImmp Communication Ontology
* VISO: VImmp Software Ontology
* VIVO: VImmp Validation Ontology
* VOV: Vimmp Ontology of Variables 

and alignment modules

* EVI: Emmo-Vimmp Integration
* EVMPO: European Virtual Marketplace Ontology
* VIPRS: VImmp PRimitiveS
* VCL: Vimmp CodeLists (for Zontal)

Examples and scenarios, combined ontology files and comprehensive documentation (.pdf) are also given.

